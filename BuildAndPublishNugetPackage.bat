rem 
rem Nuget Package Post Build Event Script
rem Copyright Sean Duffy 2015
rem 
rem This batch script is designed to work with the following Visual Studio Post build event command line :
rem 
rem "call "$(SolutionDir)packages\SeanDuffy.BuildAndPublish[current version]\BuildAndPublishNugetPackage.bat" $(ConfigurationName) "$(SolutionDir)" "$(ProjectPath)" $(TargetName) [nugetApiKey]
rem
rem Note: $(ProjectPath) can be replaced with the path to a specific nuspec file.
rem Note: [current version] must be correctly set in the command line path.
rem

set configurationName=%1
set solutionPath=%2
set projectPath=%3
set targetName=%4
set nugetApiKey=%5
set packagesFolder=packages-output

if "%nugetApiKey%" == "" goto ShowMissingParameterMessage

echo Nuget Package Builder
echo ConfigurationName 			: %configurationName%
echo SolutionDir       			: %solutionPath%
echo ProjectPath / NuspecPath  	: %projectPath%
echo TargetName        			: %targetName%
echo NugetApiKey       			: %nugetApiKey%
echo PackagesFolder    			: %packagesFolder%

if not "%configurationName%" == "Release" goto ShowIncorrectConfigurationNameMessage

cd %solutionPath%
if not %errorlevel% == 0 goto ShowInvalidSolutionDirMessage

cd %packagesFolder%
if not %errorlevel% == 0 goto ShowInvalidPackageOutputDirMessage

del *.nupkg
if not %errorlevel% == 0 goto ShowUnableToDeletePackagesMessage

nuget.exe pack %projectPath% -Prop Configuration=Release
nuget.exe push %targetName%*.nupkg %nugetApiKey% 

if not %errorlevel% == 0 goto ShowCheckForErrorsMessage

echo INFO: Nuget package publication completed with no errors
goto End

:ShowInvalidSolutionDirMessage
echo ERROR: Unable to access folder %solutionPath%
goto Error

:ShowInvalidPackageOutputDirMessage
echo ERROR: Unable to access '%packagesFolder%' folder.  Please ensure it is present in the solution folder.
goto Error

:ShowUnableToDeletePackagesMessage
echo ERROR: Unable to delete previous packages from the '%packagesFolder%' folder.
goto Error

:ShowIncorrectConfigurationNameMessage
echo INFO: Nuget package not built or published as the build was not in Release mode
goto End

:ShowMissingParameterMessage
echo ERROR: There is a missing parameter in the 'Post build event command line'. Ensure the command line is set correctly.
goto Error

:ShowCheckForErrorsMessage
echo ERROR: There was an error during package creation.  Please check the 'output' window for more information.

:Error
exit /b 1
goto End

:End
exit /b 0