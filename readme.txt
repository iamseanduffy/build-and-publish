**Nuget Package Post Build Event Script**
**Copyright Sean Duffy 2015**

The batch script that is installed by this project can be used by setting the following Visual Studio Post build event command line :

"call "$(SolutionDir)packages\SeanDuffy.BuildAndPublish.1.0.2.1\BuildAndPublishNugetPackage.bat" $(ConfigurationName) "$(SolutionDir)" "$(ProjectPath)" $(TargetName) [nugetApiKey]

Notes: 

- $(ProjectPath) can be replaced with the path to a specific nuspec file.
- If you are upgrading from a previous version of this package don't forget to update the path in the Post build event command line.

https://bitbucket.org/iamseanduffy/build-and-publish/overview
https://www.nuget.org/packages/SeanDuffy.BuildAndPublish/